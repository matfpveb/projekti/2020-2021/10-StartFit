const nodemailer = require('nodemailer');

const sendMail = async(req,res,next) =>{
    const {name,email, subject, message } = req.body;
    const transporter = nodemailer.createTransport({
        host: "smtp.gmail.com",
        port: 587,
        secure: false,
        auth: {
            user: process.env.EMAIL,
            pass: process.env.PASS
        }
    });

    transporter.verify(function (error, success) {
        if (error) {
          console.log(error);
        } else {
          console.log("Server is ready to take our messages");
        }
      });

    const mailOptions = {
        from: '',
        to: process.env.EMAIL,
        subject: subject,
        text: `FROM: ${name} <${email}> \n${message}`
    };
    try{
        const info = await transporter.sendMail(mailOptions);
        res.status(200).json(info);
    }
    catch(err){
        next(err);
    }

};
module.exports = {
    sendMail
}