const Trainer = require('../models/trainerModel');
const trainersService = require('../services/trainersService')
const Recipe = require('../models/recipeModel');

const validator = require('validator');
const bcrypt = require('bcrypt');
const jwtAuth = require('../utils/authentication');
const { uploadFile } = require('../controllers/uploadController');

const getAllTrainers = async (req, res, next) => {
    
    const page = req.query.page;
    const limit = req.query.limit;
    let filter ={};
    const options = req.body;
    console.log(options)

    if(options.city){
        filter["location.city"] = options.city;
    }
    if(options.municipality)
        filter["location.municipality"] = options.municipality;
    
    if(options.min){
        filter.approximatePrice={$gte:options.min};
    }
    if(options.max){
        filter.approximatePrice["$lte"] = options.max;
    }
       
    if(options.category)
        filter.category = {$in: options.category}
  
    
    try {
        const trainers = await paginateThroughProducts(page,limit,filter,options.sort);
        console.log(trainers);
        res.status(200).json(trainers);
    } catch (err) {
        console.log(err)
        next(err);
    }
};

const getTrainerByUsername = async (req, res, next) => {

    const username = req.params.username;
    try {
        if (!username) {
            res.status(400).json();
        }
        else {
            const trainer = await trainersService.getTrainerByUsername(username);
            if (!trainer) {
                 const error = new Error(`There is no trainer with username ${username}`);
                 error.status = 404;
                 throw error;
            }     
            else
                res.status(200).json(trainer);
        } 
    } catch(error) {
           next(error); 
    }
}

const addNewTrainer = async (req, res, next) => {

    const {username, name, surname, email, password, gender, city, municipality, dateOfBirth, imgUrl, category, approximatePrice} = req.body;

    try {

        if(!username || !email || !password || !name || !surname 
            || !city || !municipality || !dateOfBirth || !gender || !category
            || !approximatePrice
            || !validator.isEmail(email) || !validator.isAlphanumeric(username)) {

            const error = new Error('Forwarded data about trainer is not valid');
            error.status = 400;
            throw error;
        }


        console.log(category)
        categoryArr = category;

        const trainer = await trainersService.getTrainerByUsername(username);
        if (trainer) {
            const error = new Error(`Username ${username} is already taken`);
            error.status = 403;
            throw error;
        }

        

        const token = await trainersService.addNewTrainer(
            username,
            name,
            surname,
            email,
            password,
            gender,
            city,
            municipality,
            dateOfBirth,
            imgUrl,
            category,
            -1.0,
            approximatePrice
        );

        res.status(201).json({token: token});
        }catch(error){
            next(error);
            console.log(error);
        }

};

const changeTrainerPassword = async (req, res, next) => {
    const username = req.params.username;
    const {oldPassword, newPassword} = req.body;

    try {

        if (username != req.username) {
            const error = new Error(`No authorization`);
            error.status = 500;
            throw error;
        }
        
        if (!username || !oldPassword || !newPassword) {
            const error = new Error('Missing required parameter(s)');
            error.status = 400;
            throw error;
        }
        const trainer = await trainersService.getTrainerByUsername(username);
        if (!trainer) {
            const error = new Error(`There is no trainer with username ${username}`);
            error.status = 404;
            throw error;
        }

        const token = await trainersService.changeTrainerPassword(username, oldPassword, newPassword);
        
        if (token === null) {
            const error = new Error('Wrong password');
            error.status = 401;
            throw error;
        }

        return res.status(200).json({
            token: token,
          });
          
    } catch (error) {
        next(error);
    }

}


const changeTrainerScore = async (req, res, next) => {
    const username = req.params.username;

    try {  
        const trainer = await trainersService.getTrainerByUsername(username);
        if (!trainer) {
            const error = new Error("Wrong username");
            error.status = 404;
            throw error;
        }

        const updatedTrainer = await trainersService.changeTrainerScore(username);
        if (!updatedTrainer) {
            const error = new Error('Update trainer failed');
            error.status = 403;
            throw error;
        }
        else
            res.status(200).json(updatedTrainer);
    } catch (error) {
        next(error);
    }

}

const deleteTrainerByUsername = async (req, res, next) =>{

    const username = req.params.username;

    try{
        if (!username) {
            const error = new Error('Missing rusername');
            error.status = 400;
            throw error;
        }
        else {
            const trainer = await trainersService.getTrainerByUsername(username);
            if (!trainer) {
                const error = new Error(`There is no trainer with username ${username}`);
                error.status = 404;
                throw error;
            }
            await trainersService.deleteTrainerByUsername(username);
            res.status(200).json({message: `Trainer ${username} is successfully deleted`});
        }
    } catch(error){
        next(error);
    } 
};


const login = async (req, res, next) => {

    const {username, password} = req.body;
    
    try {
        if (!username || !password) {
            const error = new Error('Missing username and/or password');
            error.status = 400;
            throw error;
        }

        const trainer = await trainersService.getTrainerByUsername(username);
        if (!trainer) {
            const error = new Error(`Trainer ${username} doesn't exist`);
            error.status = 404;
            throw error;
        }

        const isMatched = await bcrypt.compare(password, trainer.password);
        if (!isMatched) {
            const error = new Error(`Wrong password`);
            error.status = 401;
            throw error;
        }
        else {
            const token = jwtAuth.generateToken({
                id: trainer._id,
                username: username,
                name: trainer.name,
                surname: trainer.surname, 
                email: trainer.email,
                gender: trainer.gender,
                city: trainer.location.city,
                municipality: trainer.location.municipality,
                dateOfBirth: trainer.dateOfBirth,
                imgUrl: trainer.imgUrl,
                category: trainer.category,
                score: trainer.score,
                approximatePrice: trainer.approximatePrice
            });
            return res.status(201).json({
                token: token      
            });
        }

    } catch (error) {
        next(error);
        console.log(error);
    }

};

const deleteFavRecipe = async(req,res,next) => {
    const {username, idRecipe} = req.params;
    try{
        const {id} = await Recipe.findOne({idRecipe: idRecipe},'_id').exec();
        const updatedTrainer = await Trainer.findOneAndUpdate(
            { username: username},
            { $pull: {recipes: id}},
             { new: true,
               useFindAndModify: false}
        ).exec();
        res.status(201).json(updatedTrainer);
    }
    catch(error){
        next(error)
    }

};

const getFavRecipes = async(req,res,next) => {
    const username = req.params.username;
    
    try {
        const recipes =  await Trainer.findOne({username: username},'recipes').populate('recipes').exec();
        res.status(200).json(recipes["recipes"]);
    } catch (error) {
        next(error)
    }

};

const addNewFavRecipe = async(req,res,next) => {
    const username = req.params.username;    
    const idRecipe = req.body.idRecipe;
   
    try {
        const {id} = await Recipe.findOne({idRecipe: idRecipe},'_id').exec();
         const updatedTrainer = await Trainer.findOneAndUpdate(
            { username: username }, 
            { $push: { recipes : id} },
            { new: true,
              useFindAndModify: false}
            ).exec();
        res.status(201).json(updatedTrainer);
         
    } catch (error) {
        next(error)
    }
};


  async function paginateThroughProducts(page = 1, limit = 10, filter, sort) {
    return await Trainer.paginate(filter, { page, limit, sort: sort});
  }

  const changeTrainerProfileImage = async (req, res, next) => {

    const username = req.username;
  
    try {
      await uploadFile(req, res);
  
      if (req.file == undefined) {
        const error = new Error('Upload a file!');
        error.status = 400;
        throw error;
      }
  
      const imgUrl = req.file.filename;
      const token = await trainersService.changeTrainerProfileImage(username, imgUrl);
  
      return res.status(200).json({
        token: token,
      });
    } catch (err) {
      next(err);
    }
  };

  

module.exports = {
    getAllTrainers,
    addNewTrainer,
    getTrainerByUsername,
    changeTrainerPassword,
    changeTrainerScore,
    deleteTrainerByUsername,
    login,
    addNewFavRecipe,
    getFavRecipes,
    deleteFavRecipe,
    changeTrainerProfileImage
};