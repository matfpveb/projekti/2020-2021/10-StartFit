const Customer = require('../models/customerModel');
const Trainer = require('../models/trainerModel');
const Nutritionist = require('../models/nutritionistModel');
const jwtAuth = require('../utils/authentication');
const mongoose = require('mongoose');
const bcrypt = require('bcrypt');



const hashPassword = async (password) => {
    const SALT_ROUNDS = 10;
    return await bcrypt.hash(password, SALT_ROUNDS);
};


const getCustomerByUsername = async (username) => {
    const customer = await Customer.findOne({username: username}).exec();
    return customer;
};

const addNewCustomer = async (username, name, surname, 
    email, password, gender,
     city, municipality, dateOfBirth, imgUrl) => {

        const hashedPassword = await hashPassword(password);

        const newCustomer = new Customer({
            _id: new mongoose.Types.ObjectId(), 
            username,
            name,
            surname,
            password: hashedPassword,
            email,
            dateOfBirth,
            gender,
            location: {
                city: city,
                municipality: municipality
            }, 
            imgUrl
        });

        await newCustomer.save();

        return jwtAuth.generateToken({
            id: newCustomer._id,
            username: username,
            name: name,
            surname: surname, 
            email: email,
            gender: gender,
            city: city,
            municipality: municipality,
            dateOfBirth: dateOfBirth,
            imgUrl: imgUrl
        });

};

const changeCustomerPassword = async (username, oldPassword, newPassword) => {

    const customer = await Customer.findOne({username:username}).exec();
    const passwordMatch = await bcrypt.compare(oldPassword, customer.password);

    if (!passwordMatch){
        return null;
    }
    const hashedPassword = await hashPassword(newPassword);
    const updatedCustomer = await Customer.findOneAndUpdate(
        { username: username },
        { $set: { password: hashedPassword } },
        { new: true });
        return jwtAuth.generateToken({
            id: updatedCustomer._id,
            username: updatedCustomer.username,
            name: updatedCustomer.name,
            surname: updatedCustomer.surname, 
            email: updatedCustomer.email,
            gender: updatedCustomer.gender,
            city: updatedCustomer.city,
            municipality: updatedCustomer.location.municipality,
            dateOfBirth: updatedCustomer.location.dateOfBirth,
            imgUrl: updatedCustomer.imgUrl
        });
};


const deleteCustomerByUsername = async (username) => {
    await Customer.findOneAndDelete({username: username}).exec();
};

const addNewTrainerToList = async(username, trainerId) => {

    const {id} = await Trainer.findOne({_id:trainerId}).exec();
        const updatedCustomer = await Customer.findOneAndUpdate(
        { username: username }, 
        { $push: { trainers : id} },
        { new: true,
            useFindAndModify: false}
        ).exec();
    return updatedCustomer;
    
};

const addNewNutritionistToList = async(username, nutritionistId) => {

    const {id} = await Nutritionist.findOne({_id:nutritionistId}).exec();
        const updatedCustomer = await Customer.findOneAndUpdate(
        { username: username }, 
        { $push: { nutritionists : id} },
        { new: true,
            useFindAndModify: false}
        ).exec();
    return updatedCustomer;
};

const getCustomersTrainerList = async(username) => { 
    const trainers =  await Customer.findOne({username: username},'trainers').populate('trainers').exec();
    return trainers['trainers'];
};

const getCustomersNutritionistList = async(username) => { 
    const nutritionists =  await Customer.findOne({username: username},'nutritionists').populate('nutritionists').exec();
    return nutritionists['nutritionists'];
};

const deleteTrainerFromCustomerTrainerList = async(username, trainerId) => {

    const {id} = await Trainer.findOne({_id:trainerId}).exec();
    const updatedCustomer = await Customer.findOneAndUpdate(
        { username: username},
        { $pull: {trainers: id}},
            { new: true,
            useFindAndModify: false}
    ).exec();

    return updatedCustomer;

};

const deleteNutritionistFromCustomerNutritionistList = async(username, nutritionistId) => {

    const {id} = await Nutritionist.findOne({_id:nutritionistId}).exec();
    const updatedCustomer = await Customer.findOneAndUpdate(
        { username: username},
        { $pull: {nutritionists: id}},
            { new: true,
            useFindAndModify: false}
    ).exec();

    return updatedCustomer;

};

async function changeCustomerProfileImage(username, imgUrl) {
    const customer = await getCustomerByUsername(username);
    customer.imgUrl = imgUrl;
    await customer.save();

    return jwtAuth.generateToken({
        id: customer._id,
        username: customer.username,
        name: customer.name,
        surname: customer.surname, 
        email: customer.email,
        gender: customer.gender,
        city: customer.location.city,
        municipality: customer.location.municipality,
        dateOfBirth: customer.dateOfBirth,
        imgUrl: customer.imgUrl
    });
};



module.exports = {
    addNewCustomer,
    getCustomerByUsername,
    deleteCustomerByUsername,
    changeCustomerPassword,
    addNewNutritionistToList,
    addNewTrainerToList,
    getCustomersTrainerList,
    getCustomersNutritionistList,
    deleteTrainerFromCustomerTrainerList,
    deleteNutritionistFromCustomerNutritionistList,
    changeCustomerProfileImage
};