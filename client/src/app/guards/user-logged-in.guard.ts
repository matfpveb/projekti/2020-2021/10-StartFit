import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { AuthUserService } from '../regular-users/services/auth-reg-user.service';
import { NutritionistAuthService } from '../health-fitness-specialists/nutritionists/services/nutritionist-auth.service';
import { TrainerAuthService } from '../health-fitness-specialists/trainers/services/trainer-auth.service';

@Injectable({
  providedIn: 'root'
})
export class UserLoggedInGuard implements CanActivate {

  constructor(private authRegUser: AuthUserService, 
    private authTrainer: TrainerAuthService, 
    private authNutritionist: NutritionistAuthService,
    private router: Router) {}
  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
      if (!this.authRegUser.userLoggedIn && !this.authNutritionist.specialistLoggedIn && !this.authTrainer.specialistLoggedIn)
      return true;
    return this.router.createUrlTree(['/']);
  }
  
}
