import { Component, Input, OnDestroy, OnInit } from '@angular/core'
import { User } from "src/app/regular-users/models/user.model";
import { Subscription, Observable } from 'rxjs';
import { HealthFitnessSpecialist } from "src/app/health-fitness-specialists/models/healthFitnessSpecialist.model";
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { RegUserService } from 'src/app/regular-users/services/reg-user.service';
import { DeleteService } from 'src/app/common/services/delete.service';
import { AuthUserService } from 'src/app/regular-users/services/auth-reg-user.service';
import { TrainerAuthService } from 'src/app/health-fitness-specialists/trainers/services/trainer-auth.service';
import { NutritionistAuthService } from 'src/app/health-fitness-specialists/nutritionists/services/nutritionist-auth.service';
import { Router } from '@angular/router';
import { TrainerService } from 'src/app/health-fitness-specialists/trainers/services/trainer.service';
import { NutritionistService } from 'src/app/health-fitness-specialists/nutritionists/services/nutritionist.service.service';

@Component({
  selector: 'app-edit-profile',
  templateUrl: './edit-profile.component.html',
  styleUrls: ['./edit-profile.component.css'],
})
export class EditProfileComponent implements OnInit {

  showChangeFields: boolean;

  @Input('user')
  public user: User;
  @Input('nutritionist')
  public nutritionist: HealthFitnessSpecialist;
  @Input('trainer')
  public trainer: HealthFitnessSpecialist;


  passForm: FormGroup;
  imgForm: FormGroup;
  private activeSubscriptions: Subscription[] = [];
  private imageToUpload: File;

  constructor(private userService: RegUserService, 
    private trainerService: TrainerService,
    private nutritionistService: NutritionistService,
    private deleteService: DeleteService,
    private userAuth: AuthUserService,
    private trainerAuth: TrainerAuthService,
    private nutritionistAuth: NutritionistAuthService,
    private router: Router) {

      this.passForm = new FormGroup({
        oldPassword: new FormControl("", [Validators.required, Validators.minLength(8)]),
        newPassword: new FormControl("", [Validators.required, Validators.minLength(8)])
      });
      this.imgForm = new FormGroup({
        imgUrl: new FormControl("",[])
      });

      this.disableChangeFields();

  }

  public deleteProfile() {
    if(this.userIn)
    this.deleteService.deleteProfile(this.user.username, "customers").subscribe((data)=>{
    console.log(data);
    this.userAuth.logoutUser();
    this.router.navigateByUrl("/");
   });
  else if(this.trainerIn)
    this.deleteService.deleteProfile(this.trainer.username, "trainers").subscribe((data)=>{
    console.log(data);
    this.trainerAuth.logoutSpecialist();
    this.router.navigateByUrl("/");
   });
  else
  this.deleteService.deleteProfile(this.trainer.username, "nutritionists").subscribe((data)=>{
    console.log(data);
    this.nutritionistAuth.logoutSpecialist();
    this.router.navigateByUrl("/");
   });
  }

  public handleFileInput(event: Event): void {
    const files: FileList = (event.target as HTMLInputElement).files;
    if (!files.length) {
      this.imageToUpload = null;
      return;
    }
    this.imageToUpload = files.item(0);
  }

  public changePicture(): void {

    if (this.imageToUpload && this.userIn) {
      const sub: Subscription = this.userService
        .patchUserProfileImage(this.imageToUpload)
        .subscribe((user: User) => {
          this.user = user;
          this.imageToUpload = null;
        });
    }

    else if (this.imageToUpload && this.nutritionistIn) {
      const sub: Subscription = this.nutritionistService
        .patchSpecialistProfileImage(this.imageToUpload)
        .subscribe((nutritionist: HealthFitnessSpecialist) => {
          this.nutritionist = nutritionist;
          console.log(nutritionist);
          this.imageToUpload = null;
        });
    }
    
    else if (this.imageToUpload && this.trainerIn) {
      const sub: Subscription = this.trainerService
        .patchSpecialistProfileImage(this.imageToUpload)
        .subscribe((trainer: HealthFitnessSpecialist) => {
          this.trainer = trainer;
          this.imageToUpload = null;
        });
    }
  }

  public changePassword(): void {

    if (this.passForm.invalid) {
      window.alert("The form is not valid!");
      return;
    }

    const formData = this.passForm.value;
    console.log(formData);

    if (this.userIn) {
      const sub: Subscription = this.userService
        .patchUserPassword(this.user.username, formData.oldPassword, formData.newPassword)
        .subscribe((user: User) => {
          if (!user)
            return;
          this.user = user;
        });
    }

    else if (this.nutritionistIn) {
      const sub: Subscription = this.nutritionistService
        .patchSpecialistPassword(this.nutritionist.username, formData.oldPassword, formData.newPassword)
        .subscribe((nutritionist: HealthFitnessSpecialist) => {
          if (!nutritionist)
            return;
          this.nutritionist = nutritionist;
        });
      }

    else if (this.trainerIn) {
      const sub: Subscription = this.trainerService
        .patchSpecialistPassword(this.trainer.username, formData.oldPassword, formData.newPassword)
        .subscribe((trainer: HealthFitnessSpecialist) => {
          if (!trainer)
            return;
          this.trainer = trainer;
        });
      }
  }



  

  private get userIn(): boolean {
    return this.user !== null;
  }

  private get nutritionistIn(): boolean {
    return this.nutritionist !== null;
  }

  private get trainerIn(): boolean {
    return this.trainer !== null;
  }


  ngOnInit() {}

  enableChangeFields() {
   this.showChangeFields = !this.showChangeFields;
  }

  disableChangeFields() {
    this.showChangeFields = false;
  }

  onChangeName(event: Event) {
    const newName: string = (event.target as HTMLInputElement).value;
    this.user.name = newName;
  }
  getColorForCategory(indeks):string{
    return indeks? "#d96846": "#013328";
  }
}
