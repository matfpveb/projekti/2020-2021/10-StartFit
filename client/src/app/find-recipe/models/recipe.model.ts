export class Recipe {
   
    constructor(    
      public id: number,
      public title: string,
      public image: string,
      public imageType: string,
      public usedIngredientCount: number,
      public missedIngredientCount: number,
      public missedIngredients: any
      ){
      
    }
    
  }
  