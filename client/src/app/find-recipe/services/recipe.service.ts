import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable, Subscription } from 'rxjs';
import { Recipe } from '../models/recipe.model';
import { RecipePagination, ApiRecipe, ApiRecipeInfo } from '../models/recipe.pagination';
import {map} from 'rxjs/operators'
import { RecipeInfo, IRecipeInfo, Joke, ApiJoke} from '../models/recipeInfo.model';
import { JwtService } from 'src/app/common/services/jwt.service';
import { TokenId } from 'src/app/common/services/models/jwt-token-ids';
import { User } from 'src/app/regular-users/models/user.model';

@Injectable({
  providedIn: 'root'
})
export class RecipeService {
  private url: string = "http://localhost:3000/api/recipes/";

  constructor(private http : HttpClient, private jwtService : JwtService) {}

  public getRecipes(ingredients : string) : Observable<Recipe[]>
  {
    const urlReq : string = this.url.concat("ingredients/").concat(ingredients);

    const obs : Observable<Recipe[]> =
    this.http.get<ApiRecipe[]>(urlReq)
    .pipe(
      map((docs : (ApiRecipe[])) => {
        return docs.map (
          (doc : ApiRecipe) =>
          new Recipe(
              doc.id,
              doc.title,
              doc.image,
              doc.imageType,
              doc.usedIngredientCount,
              doc.missedIngredientCount,
              doc.missedIngredients
            )

        );
      })
    );
    return obs;

    }

    public getRecipeInfo(id : number) : Observable<RecipeInfo> {

      var idS : string = id.toString();
      const urlReq = this.url.concat(idS);
      console.log(urlReq);

      const obs : Observable<RecipeInfo> =
      this.http.get<ApiRecipeInfo>(urlReq)
      .pipe(
          map (
            (doc : ApiRecipeInfo) =>
            new RecipeInfo(
              doc.id.toString(),
              doc.title,
              doc.summary,
              doc.extendedIngredients,
              doc.instructions,
              doc.readyInMinutes,
              doc.healthScore,
              doc.dishTypes,
              doc.image,
              doc.vegeterian,
              doc.vegan,
              doc.glFree,
              doc.dairyFree
              )          
          )
      );

      return obs;

    }

    public postRecipe(recipe: RecipeInfo, url : string){
      recipe.ingredients = [""];
      console.log(url);
      //const headers: HttpHeaders = new HttpHeaders().append("Authorization", `Bearer ${this.jwtService.getToken(TokenId.USER_ID)}`);
      this.http
        .put<any>(url, 
      recipe).subscribe(data => console.log(data));
    }

    public getJoke() : Observable<Joke>  {
      const url = "https://spoonacular-recipe-food-nutrition-v1.p.rapidapi.com/food/jokes/random";

      const headers: HttpHeaders = new HttpHeaders().append('x-rapidapi-key', "536a175d74msh2ab8ce7ace1d089p18c7fajsneb219192a386");
      headers.append('x-rapidapi-host', 'spoonacular-recipe-food-nutrition-v1.p.rapidapi.com');
      const obs : Observable<Joke> = 
      this.http.get<ApiJoke>(url, {headers})
      .pipe(
        map (
          (doc : ApiJoke) =>
          new Joke(
            doc.text  
            )          
        )
      );

     return obs;
    }
    
   
}


