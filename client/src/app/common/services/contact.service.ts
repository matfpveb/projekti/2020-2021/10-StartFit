import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from "rxjs";

@Injectable({
  providedIn: 'root'
})
export class ContactService {

  private readonly sendMailsUrl = 'http://localhost:3000/api/contactForm/sendmail'

  constructor(private http: HttpClient) { }

  sendMail(formData): Observable<{}>{
    const body = {
      name: formData.name,
      email: formData.email,
      subject: formData.subject,
      message: formData.message
    }
    return this.http.post(this.sendMailsUrl,body)
  }
}
