import { Component, OnInit } from '@angular/core';
import { FormArray, FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { Observable } from 'rxjs';
import { HealthFitnessSpecialist } from '../health-fitness-specialists/models/healthFitnessSpecialist.model';
import {NutritionistCategory} from '../health-fitness-specialists/models/healthFitnessSpecialistCategories'
import { NutritionistService } from '../health-fitness-specialists/nutritionists/services/nutritionist.service.service';
import { Router } from '@angular/router';
declare const $: any;

@Component({
  selector: 'app-find-nutritionist',
  templateUrl: './find-nutritionist.component.html',
  styleUrls: ['./find-nutritionist.component.css']
})
export class FindNutritionistComponent implements OnInit {


  categories: Array<any> = Object.values(NutritionistCategory);
  nutritionist: Observable<HealthFitnessSpecialist[]>;
  cat: [string];

  findNutritionistForm: FormGroup;
  selectedCategories: [string];
  sort: string;

  get categoriesFormArray() {
    return this.findNutritionistForm.controls.categories as FormArray;
  }

  constructor(private formBuilder: FormBuilder, private nutritionistService: NutritionistService, private router: Router) {
    this.findNutritionistForm = formBuilder.group({
      minPrice: new FormControl("", []),
      maxPrice: new FormControl("", []),
      city: new FormControl("", []),
      municipality: new FormControl("", []),
      categories: new FormArray([]),
      sort: new FormControl(false, [])
  });

  this.addCheckboxes();

   }

  ngOnInit() {
    $('.menu .item').tab();
  }

  public findNutritionists(): void {}

  private addCheckboxes() {
    this.categories.forEach(() => this.categoriesFormArray.push(new FormControl(false)));
  }
  // submit() {
  //   /* ovo je niz svih kategorija koje je korisnik odabrao*/
  //   const selectedCategories = this.findNutritionistForm.value.categories
  //     .map((checked, i) => checked ? this.categories[i] : null)
  //     .filter(v => v !== null);
  //     console.log(selectedCategories);

  //     let sort="";
  //     if(this.findNutritionistForm.value.sort)
  //       sort = "score";

  //     this.nutritionist = this.nutritionistService.getNutritionists(1,10,
  //         this.findNutritionistForm.value.minPrice,
  //         this.findNutritionistForm.value.maxPrice,
  //         this.findNutritionistForm.value.city,
  //         this.findNutritionistForm.value.municipality,
  //         selectedCategories,
  //         sort)
  //     console.log(this.nutritionist);
  //     this.nutritionist.subscribe()

  // }

  submit() {
    /* ovo je niz svih kategorija koje je korisnik odabrao*/
      this.selectedCategories = this.findNutritionistForm.value.categories
        .map((checked, i) => checked ? this.categories[i] : null)
        .filter(v => v !== null);

      this.sort="";
      if(this.findNutritionistForm.value.sort)
        this.sort = "score";

      const length: number = this.selectedCategories.length;

      if (length > 0) {
        this.router.navigate(['/nutritionists/nutritionists-list'], {
          queryParams: {
            min:this.findNutritionistForm.value.minPrice,
            max:this.findNutritionistForm.value.maxPrice,
            city:this.findNutritionistForm.value.city,
            municipality:this.findNutritionistForm.value.municipality,
            categories:this.selectedCategories,
            sort:this.sort
          }
        });
      }
      else {
        this.router.navigate(['/nutritionists/nutritionists-list'], {
          queryParams: {
            min:this.findNutritionistForm.value.minPrice,
            max:this.findNutritionistForm.value.maxPrice,
            city:this.findNutritionistForm.value.city,
            municipality:this.findNutritionistForm.value.municipality,
            categories:this.cat,
            sort:this.sort
          }
        });
      }
  }
}
