import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule} from '@angular/forms';
import { MatInputModule,
         MatCardModule,
         MatToolbarModule,
         MatButtonModule,
         MatIconModule,
         MatSnackBarModule} from '@angular/material';

import { AppRoutingModule, routingComponents } from './app-routing.module';
import { AppComponent } from './app.component';
import { HeaderComponent } from './common/header/header.component';
import { FooterComponent } from './common/footer/footer.component';
import { HomeComponent } from './common/home/home.component';
import { FindRecipeComponent } from './find-recipe/find-recipe.component';
import { ContactComponent } from './common/contact/contact.component';
import { EditProfileComponent } from './profile/edit-profile/edit-profile.component';
import { AboutComponent } from './common/about/about.component';
import { FindTrainerComponent } from './find-trainer/find-trainer.component';
import { FindNutritionistComponent } from './find-nutritionist/find-nutritionist.component';
import { HttpClientModule } from '@angular/common/http';
import { SignupComponent } from './signup/signup.component';
import { SigninComponent } from './signin/signin.component';
import { SignupTrainerComponent } from './health-fitness-specialists/trainers/signup-trainer/signup-trainer.component';
import { SignupRegularUserComponent } from './regular-users/signup-regular-user/signup-regular-user.component';
import { SignupNutritionistComponent } from './health-fitness-specialists/nutritionists/signup-nutritionist/signup-nutritionist.component';
import { TrainersListComponent } from './health-fitness-specialists/trainers/trainers-list/trainers-list.component';
import { TrainerInfoComponent } from './health-fitness-specialists/trainers/trainer-info/trainer-info.component';
import { RecipeListComponent } from './find-recipe/recipe-list/recipe-list.component';
import { RecipeComponent } from './find-recipe/recipe/recipe.component';
import { NutritionistInfoComponent } from './health-fitness-specialists/nutritionists/nutritionist-info/nutritionist-info.component';
import { NutritionistsListComponent } from './health-fitness-specialists/nutritionists/nutritionists-list/nutritionists-list.component';
import { ProfileComponent } from './profile/profile.component';
import { SigninTrainerComponent } from './health-fitness-specialists/trainers/signin-trainer/signin-trainer.component';
import { SigninNutritionistComponent } from './health-fitness-specialists/nutritionists/signin-nutritionist/signin-nutritionist.component';
import { SigninRegularUserComponent } from './regular-users/signin-regular-user/signin-regular-user.component';
import { RecipeInfoComponent } from './find-recipe/recipe-info/recipe-info.component';
import { FavoriteTrainersComponent } from './profile/favorite-trainers/favorite-trainers.component';
import { FavoriteNutritionistsComponent } from './profile/favorite-nutritionists/favorite-nutritionists.component';
import { FavoriteRecipesComponent } from './profile/favorite-recipes/favorite-recipes.component';


@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    FooterComponent,
    HomeComponent,
    routingComponents,
    FindRecipeComponent,
    ContactComponent,
    AboutComponent,
    FindTrainerComponent,
    FindNutritionistComponent,
    SignupComponent,
    SigninComponent,
    SignupTrainerComponent,
    SignupRegularUserComponent,
    SignupNutritionistComponent,
    TrainersListComponent,
    TrainerInfoComponent,
    FindNutritionistComponent,
    RecipeListComponent,
    RecipeComponent,
    NutritionistInfoComponent,
    NutritionistsListComponent,
    FindNutritionistComponent,
    ProfileComponent,
    EditProfileComponent,
    SigninTrainerComponent,
    SigninNutritionistComponent,
    RecipeInfoComponent,
    SigninRegularUserComponent,
    FavoriteTrainersComponent,
    FavoriteNutritionistsComponent,
    FavoriteRecipesComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    MatInputModule,
    MatCardModule,
    MatToolbarModule,
    MatButtonModule,
    ReactiveFormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    MatIconModule,
    MatSnackBarModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
