import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { AuthUserService } from '../services/auth-reg-user.service';

@Component({
  selector: 'app-signin-regular-user',
  templateUrl: './signin-regular-user.component.html',
  styleUrls: ['./signin-regular-user.component.css']
})
export class SigninRegularUserComponent implements OnInit, OnDestroy {

  regularUserSigninForm: FormGroup;
  signInSub: Subscription;

  constructor(private authService: AuthUserService,
    private router: Router) {
    this.regularUserSigninForm = new FormGroup({
      username: new FormControl("", [Validators.required]),
      password: new FormControl("", [Validators.required])
    });
  }

  ngOnInit() {
  }

  public ngOnDestroy(): void {

  }

  public signIn(): void {

    if (this.regularUserSigninForm.invalid) {
      window.alert("The form is not valid!");
      return;
    }

    const data = this.regularUserSigninForm.value;
    this.signInSub = this.authService.signInUser(data.username, data.password).subscribe(() => {
      this.router.navigateByUrl("/");
    });
    
  }

  focusUsername(): string{
    if(this.regularUserSigninForm.value.username)
      return 'focus';
    return 'blur';
  }
  focusPassword(): string{
    if(this.regularUserSigninForm.value.password)
      return 'focus';
    return 'blur';
  }
}
