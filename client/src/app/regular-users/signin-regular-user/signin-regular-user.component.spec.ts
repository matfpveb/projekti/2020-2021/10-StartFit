import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SigninRegularUserComponent } from './signin-regular-user.component';

describe('SigninRegularUserComponent', () => {
  let component: SigninRegularUserComponent;
  let fixture: ComponentFixture<SigninRegularUserComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SigninRegularUserComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SigninRegularUserComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
