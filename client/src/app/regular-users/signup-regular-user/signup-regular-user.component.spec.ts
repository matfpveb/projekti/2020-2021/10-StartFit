import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SignupRegularUserComponent } from './signup-regular-user.component';

describe('SignupRegularUserComponent', () => {
  let component: SignupRegularUserComponent;
  let fixture: ComponentFixture<SignupRegularUserComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SignupRegularUserComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SignupRegularUserComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
