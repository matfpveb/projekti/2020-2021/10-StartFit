import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators, ValidationErrors, FormArray } from '@angular/forms';
import { Router } from '@angular/router';
import { Observable, Subscription } from 'rxjs';
import { TokenId } from 'src/app/common/services/models/jwt-token-ids';
import { HealthFitnessSpecialist } from '../../models/healthFitnessSpecialist.model';
import { NutritionistCategory } from '../../models/healthFitnessSpecialistCategories';
import { NutritionistAuthService } from '../services/nutritionist-auth.service';

declare const $: any;

@Component({
  selector: 'app-signup-nutritionist',
  templateUrl: './signup-nutritionist.component.html',
  styleUrls: ['./signup-nutritionist.component.css']
})
export class SignupNutritionistComponent implements OnInit {


  nutritionist: HealthFitnessSpecialist;
  somethingIsNotValidNutritionist: boolean = false;
  allIsValidNutritionist: boolean = true;
  nutrSub: Subscription;
  categories: Array<any> = Object.values(NutritionistCategory);
  passwordNotConfirmed: boolean = false;

  get categoriesFormArray() {
    return this.nutritionistForm.controls.categories as FormArray;
  }

  constructor(private authService: NutritionistAuthService,
    private router: Router) {

    this.nutritionistForm = new FormGroup({
      name: new FormControl("", [Validators.required, Validators.minLength(2)]),
      surname: new FormControl("", [Validators.required, Validators.minLength(2)]),
      username: new FormControl("", [Validators.required, Validators.minLength(2)]),
      email: new FormControl("", [Validators.required, Validators.email]),
      city: new FormControl("", [Validators.required]),
      municipality: new FormControl("", [Validators.required]),
      dateOfBirth: new FormControl("", [Validators.required]),
      password: new FormControl("", [Validators.required, Validators.minLength(8)]),
      cnfPassword: new FormControl("", [Validators.required, Validators.minLength(8)]),
      gender: new FormControl("", [Validators.required]),
      categories: new FormArray([]),
      price: new FormControl("", [Validators.required, Validators.min(1)])

    });
    this.addCheckboxes();

    this.authService.tokenId = TokenId.NUTRITIONIST_ID;
  }

  nutritionistForm: FormGroup;

  ngOnInit() {
    $('.ui.radio.checkbox').checkbox();
  }

  private addCheckboxes() {
    this.categories.forEach(() => this.categoriesFormArray.push(new FormControl(false)));
  }


  public register(): void {

    this.checkPasswords(this.nutritionistForm);

    if(this.nutritionistForm.invalid || this.passwordNotConfirmed) {
      this.somethingIsNotValidNutritionist = true;
      this.allIsValidNutritionist= false;
      console.log(this.nutritionistForm.value);
      window.alert('Form is not valid. Please try again!');
      return;
    }

    const selectedCategories = this.nutritionistForm.value.categories
    .map((checked, i) => checked ? this.categories[i] : null)
    .filter(v => v !== null);
    console.log(selectedCategories);

    this.somethingIsNotValidNutritionist = false;
    this.allIsValidNutritionist = true;

    var img: string;
    if (this.nutritionistForm.value.gender == 'male')
        img = 'male_avatar.png';
    else if (this.nutritionistForm.value.gender == 'female')
      img = 'female_avatar.png'
    else
      img = 'other_avatar.png'


    const obsNutr: Observable<HealthFitnessSpecialist> =  this.authService.registerSpecialist(
      this.nutritionistForm.value.username,
      this.nutritionistForm.value.password,
      this.nutritionistForm.value.name,
      this.nutritionistForm.value.surname,
      this.nutritionistForm.value.email,
      this.nutritionistForm.value.gender,
      this.nutritionistForm.value.city,
      this.nutritionistForm.value.municipality,
      this.nutritionistForm.value.dateOfBirth,
      img,
      selectedCategories,
      this.nutritionistForm.value.price
      );

      //if (this.trainerSub)
        //this.trainerSub.unsubscribe();

      this.nutrSub = obsNutr.subscribe(() => {
        this.router.navigateByUrl("/");
      });
  }

  checkPasswords(group: FormGroup) {
    const password = group.get('password').value;
    const cnfPassword = group.get('cnfPassword').value;
    if (password === cnfPassword) {
      this.passwordNotConfirmed = false;
    }
    else {
      this.passwordNotConfirmed = true;
    }
  }

  public nameHasErrors(): boolean {
    const errors: ValidationErrors = this.nutritionistForm.get('name').errors;

    return errors !== null;
  }

  public nameErrors(): string[] {
    const errors: ValidationErrors = this.nutritionistForm.get('name').errors;
    if(errors === null) {
      return [];
    }

    const errorMessages: string[] = [];
    if (errors.required) {
      errorMessages.push('Nutritionist must have a first name.');
    }

    return errorMessages;
  }

  surnameHasErrors(): boolean {
    const errors: ValidationErrors = this.nutritionistForm.get('surname').errors;

    return errors !== null;
  }

  usernameHasErrors(): boolean {
    const errors: ValidationErrors = this.nutritionistForm.get('username').errors;

    return errors !== null;
  }

  municipalityHasErrors(): boolean {
    const errors: ValidationErrors = this.nutritionistForm.get('municipality').errors;

    return errors !== null;
  }

  cityHasErrors(): boolean {
    const errors: ValidationErrors = this.nutritionistForm.get('city').errors;

    return errors !== null;
  }

  emailHasErrors(): boolean {
    const errors: ValidationErrors = this.nutritionistForm.get('email').errors;

    return errors !== null;
  }

  dateOfBirthHasErrors(): boolean {
    const errors: ValidationErrors = this.nutritionistForm.get('dateOfBirth').errors;

    return errors !== null;
  }

  passwordHasErrors(): boolean {
    const errors: ValidationErrors = this.nutritionistForm.get('password').errors;

    return errors !== null;
  }

  cnfPasswordHasErrors(): boolean {
    const errors: ValidationErrors = this.nutritionistForm.get('cnfPassword').errors;

    return errors !== null;
  }

  genderHasErrors(): boolean {
    const errors: ValidationErrors = this.nutritionistForm.get('gender').errors;

    return errors !== null;
  }

  categoryHasErrors(): boolean {
    const errors: ValidationErrors = this.nutritionistForm.get('category').errors;

    return errors !== null;
  }

  public priceHasErrors(): boolean {
    const errors: ValidationErrors = this.nutritionistForm.get('price').errors;
    console.log(errors)

    return errors !== null;
  }


  public surnameErrors(): string[] {
    const errors: ValidationErrors = this.nutritionistForm.get('surname').errors;
    if(errors === null) {
      return [];
    }

    const errorMessages: string[] = [];
    if (errors.required) {
      errorMessages.push('Nutritionist must have a last name.');
    }

    return errorMessages;
  }

  public usernameErrors(): string[] {
    const errors: ValidationErrors = this.nutritionistForm.get('username').errors;
    if(errors === null) {
      return [];
    }

    const errorMessages: string[] = [];
    if (errors.required) {
      errorMessages.push('Nutritionist must have a username.');
    }

    return errorMessages;
  }

  public emailErrors(): string[] {
    const errors: ValidationErrors = this.nutritionistForm.get('email').errors;
    if(errors === null) {
      return [];
    }

    const errorMessages: string[] = [];
    if (errors.required) {
      errorMessages.push('Nutritionist must have an email.');
    }
    if (errors.email) {
      errorMessages.push('Email must have a valid format.');
    }

    return errorMessages;
  }

  public municipalityErrors(): string[] {
    const errors: ValidationErrors = this.nutritionistForm.get('municipality').errors;
    if(errors === null) {
      return [];
    }

    const errorMessages: string[] = [];
    if (errors.required) {
      errorMessages.push('Nutritionist must have a municipality.');
    }

    return errorMessages;
  }

  public cityErrors(): string[] {
    const errors: ValidationErrors = this.nutritionistForm.get('city').errors;
    if(errors === null) {
      return [];
    }

    const errorMessages: string[] = [];
    if (errors.required) {
      errorMessages.push('Nutritionist must have a city.');
    }

    return errorMessages;
  }

  public dateOfBirthErrors(): string[] {
    const errors: ValidationErrors = this.nutritionistForm.get('dateOfBirth').errors;
    if(errors === null) {
      return [];
    }

    const errorMessages: string[] = [];
    if (errors.required) {
      errorMessages.push('Nutritionist must have a dateOfBirth of birth.');
    }

    return errorMessages;
  }

  public passwordErrors(): string[] {
    const errors: ValidationErrors = this.nutritionistForm.get('password').errors;
    if(errors === null) {
      return [];
    }

    const errorMessages: string[] = [];
    if (errors.required) {
      errorMessages.push('Nutritionist must have a first name.');
    }

    return errorMessages;
  }

  public cnfPasswordErrors(): string[] {
    const errors: ValidationErrors = this.nutritionistForm.get('cnfPassword').errors;
    if(errors === null) {
      return [];
    }

    const errorMessages: string[] = [];
    if (errors.required) {
      errorMessages.push('Password must be confirmed.');
    }

    return errorMessages;
  }

  public genderErrors(): string[] {
    const errors: ValidationErrors = this.nutritionistForm.get('gender').errors;
    if(errors === null) {
      return [];
    }

    const errorMessages: string[] = [];
    if (errors.required) {
      errorMessages.push('Gender must be choosen.');
    }

    return errorMessages;
  }

  public categoryErrors(): string[] {
    const errors: ValidationErrors = this.nutritionistForm.get('category').errors;
    if(errors === null) {
      return [];
    }

    const errorMessages: string[] = [];
    if (errors.required) {
      errorMessages.push('Nutritionist must have a category.');
    }

    return errorMessages;
  }
  public priceErrors(): string[] {
    const errors: ValidationErrors = this.nutritionistForm.get('price').errors;
    if(errors === null) {
      return [];
    }

    const errorMessages: string[] = [];
    if (errors.required) {
      errorMessages.push('Nutritionist must have a price');
    }
    if(errors.min){
      errorMessages.push('Price must be positive');
    }

    return errorMessages;
  }
}
