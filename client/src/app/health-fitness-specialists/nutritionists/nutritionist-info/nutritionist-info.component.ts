import { Component, OnInit, Input } from '@angular/core';
import { HealthFitnessSpecialist, HealthFitnessSpecialistRating } from 'src/app/health-fitness-specialists/models/healthFitnessSpecialist.model';
import { Subscription } from 'rxjs';
import { AuthUserService } from 'src/app/regular-users/services/auth-reg-user.service';
import { Router } from '@angular/router';
import { User } from 'src/app/regular-users/models/user.model';
import { NutritionistService } from '../services/nutritionist.service.service';
import { MatSnackBar } from '@angular/material/snack-bar';

declare const $: any;

@Component({
  selector: 'app-nutritionist-info',
  templateUrl: './nutritionist-info.component.html',
  styleUrls: ['./nutritionist-info.component.css']
})
export class NutritionistInfoComponent implements OnInit {

  RatingEnum = HealthFitnessSpecialistRating;

  @Input() nutritionist: HealthFitnessSpecialist;

  private user: User = null;
  private userSub: Subscription;
  public isSomeoneLoggedIn: boolean;
  addingIsPossible: boolean = true;
  ratingIsPossible: boolean = true;
  rating: number = 0;
  starCount: number = 5;
  ratingArr: boolean[] = [];
  snackBarDuration: number = 2000;
  response = [
    1,
    2,
    3,
    4,
    5
  ];


  constructor(private snackBar: MatSnackBar, private nutritionistService: NutritionistService, private authUserService: AuthUserService, private router: Router) {
    this.ratingArr = Array(this.starCount).fill(false);

    //user
    this.userSub = this.authUserService.user.subscribe((user: User) => {
      this.user = user;
    });
    this.authUserService.sendUserDataIfExists();

    this.setisSomeoneLoggedIn();
  }

  returnStar(i: number) {
    if(this.rating >= i+1){
      return 'star';
    }
    else {
      return 'star_border';
    }
  }

  onClick(i: number) {
    if(this.ratingIsPossible){
      this.rating = i + 1;
      this.snackBar.open('You rated the nutritionist ' + this.nutritionist.name + ' ' + this.nutritionist.surname + ' with a rating of ' + this.response[i].toString(),'', {
        duration: this.snackBarDuration,
        panelClass: ['snack-bar']
      });
      this.ratingIsPossible = false;
      this.nutritionistService.rateNutritionist(this.nutritionist.username, this.response[i]);
    }
  }

  public nutritionistRating(): HealthFitnessSpecialistRating {

    if (this.nutritionist.marks === 0 ) {
      return HealthFitnessSpecialistRating.NotRated;
    }
    if (this.nutritionist.marks/this.nutritionist.numberOfCustomer > 4) {
      return HealthFitnessSpecialistRating.Excellent;
    }
    if (this.nutritionist.marks/this.nutritionist.numberOfCustomer > 3 ) {
      return HealthFitnessSpecialistRating.Good;
    }
    if (this.nutritionist.marks/this.nutritionist.numberOfCustomer > 2 ) {
      return HealthFitnessSpecialistRating.Average;
    }
    return HealthFitnessSpecialistRating.Bad;
  }

  private setisSomeoneLoggedIn() : void {
    if (this.authUserService.userLoggedIn)
      this.isSomeoneLoggedIn = true;
    else
      this.isSomeoneLoggedIn = false;
  }

  public logout(): void {
    if (this.authUserService.userLoggedIn) {
      this.authUserService.logoutUser();
      this.isSomeoneLoggedIn = false;
    }
    this.router.navigateByUrl("/");
  }

  ngOnDestroy() {
    this.userSub ? this.userSub.unsubscribe() : null;
  }
  ngOnInit() {
    $('.ui.rating').rating();
  }

  public nutritionistAddToFavorits(){
    this.addingIsPossible = false;
    this.nutritionistService.postNutritionist(this.nutritionist.id, this.user.username);
  }
  getColorForCategory(indeks):string{
    return indeks? "#d96846": "#013328";
  }
}
