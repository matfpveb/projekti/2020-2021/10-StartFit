import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { JwtService } from '../../../common/services/jwt.service';
import { TokenId } from '../../../common/services/models/jwt-token-ids';
import { HealthFitnessSpecialistAuth } from '../../services/health-fitness-specialist-auth.service';

@Injectable({
  providedIn: 'root'
})
export class NutritionistAuthService extends HealthFitnessSpecialistAuth{

  constructor(http: HttpClient, jwtService: JwtService) {
    super(http, jwtService);
    super.tokenId = TokenId.NUTRITIONIST_ID;
    super.registerUrl = "http://localhost:3000/api/nutritionists/register/";
    super.loginUrl = "http://localhost:3000/api/nutritionists/login/";
   }
}
