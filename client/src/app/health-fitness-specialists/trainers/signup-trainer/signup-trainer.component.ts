import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators, ValidationErrors, FormArray } from '@angular/forms';
import { Router } from '@angular/router';
import { Observable, Subscription } from 'rxjs';
import { HealthFitnessSpecialist } from '../../models/healthFitnessSpecialist.model';
import { TrainerCategory } from '../../models/healthFitnessSpecialistCategories';
import { TrainerAuthService } from '../services/trainer-auth.service';

declare const $: any;

@Component({
  selector: 'app-signup-trainer',
  templateUrl: './signup-trainer.component.html',
  styleUrls: ['./signup-trainer.component.css']
})
export class SignupTrainerComponent implements OnInit {

  trainer: HealthFitnessSpecialist;
  somethingIsNotValidTrainer: boolean = false;
  allIsValidTrainer: boolean = true;
  trainerSub: Subscription;
  categories: Array<any> = Object.values(TrainerCategory);
  passwordNotConfirmed: boolean = false;

  get categoriesFormArray() {
    return this.trainerForm.controls.categories as FormArray;
  }
  constructor(private authService: TrainerAuthService,
    private router: Router) {
    this.trainerForm = new FormGroup({
      name: new FormControl("", [Validators.required, Validators.minLength(2)]),
      surname: new FormControl("", [Validators.required, Validators.minLength(2)]),
      username: new FormControl("", [Validators.required, Validators.minLength(2)]),
      email: new FormControl("", [Validators.required, Validators.email]),
      city: new FormControl("", [Validators.required]),
      municipality: new FormControl("", [Validators.required]),
      dateOfBirth: new FormControl("", [Validators.required]),
      password: new FormControl("", [Validators.required, Validators.minLength(8)]),
      cnfPassword: new FormControl("", [Validators.required, Validators.minLength(8)]),
      gender: new FormControl("", [Validators.required]),
      price: new FormControl("", [Validators.required, Validators.min(1)]),
      categories: new FormArray([])
    });
    this.addCheckboxes();
  }


  trainerForm: FormGroup;

  ngOnInit() {
    $('.ui.radio.checkbox').checkbox();

  }
  private addCheckboxes() {
    this.categories.forEach(() => this.categoriesFormArray.push(new FormControl(false)));
  }
  public register(): void {

    this.checkPasswords(this.trainerForm);

    if(this.trainerForm.invalid || this.passwordNotConfirmed) {
      this.somethingIsNotValidTrainer = true;
      this.allIsValidTrainer = false;
      console.log(this.trainerForm.value);
      window.alert('Form is not valid. Please try again!');
      return;
    }

    const selectedCategories = this.trainerForm.value.categories
    .map((checked, i) => checked ? this.categories[i] : null)
    .filter(v => v !== null);
    console.log(selectedCategories);

    this.somethingIsNotValidTrainer = false;
    this.allIsValidTrainer = true;

    var img: string;
    if (this.trainerForm.value.gender == 'male')
        img = 'male_avatar.png';
    else if (this.trainerForm.value.gender == 'female')
      img = 'female_avatar.png'
    else
      img = 'other_avatar.png'

    console.log(this.trainerForm.value);

    const obsTrainer: Observable<HealthFitnessSpecialist> =  this.authService.registerSpecialist(
      this.trainerForm.value.username,
      this.trainerForm.value.password,
      this.trainerForm.value.name,
      this.trainerForm.value.surname,
      this.trainerForm.value.email,
      this.trainerForm.value.gender,
      this.trainerForm.value.city,
      this.trainerForm.value.municipality,
      this.trainerForm.value.dateOfBirth,
      img,
      selectedCategories,
      this.trainerForm.value.price
      );

      //if (this.trainerSub)
        //this.trainerSub.unsubscribe();

      this.trainerSub = obsTrainer.subscribe(() => {
        this.router.navigateByUrl("/");
      });

  }

  checkPasswords(group: FormGroup) {
    const password = group.get('password').value;
    const cnfPassword = group.get('cnfPassword').value;
    if (password === cnfPassword) {
      this.passwordNotConfirmed = false;
    }
    else {
      this.passwordNotConfirmed = true;
    }
  }

  public nameHasErrors(): boolean {
    const errors: ValidationErrors = this.trainerForm.get('name').errors;

    return errors !== null;
  }

  public nameErrors(): string[] {
    const errors: ValidationErrors = this.trainerForm.get('name').errors;
    if(errors === null) {
      return [];
    }

    const errorMessages: string[] = [];
    if (errors.required) {
      errorMessages.push('Trainer must have a first name.');
    }

    return errorMessages;
  }

  surnameHasErrors(): boolean {
    const errors: ValidationErrors = this.trainerForm.get('surname').errors;

    return errors !== null;
  }

  usernameHasErrors(): boolean {
    const errors: ValidationErrors = this.trainerForm.get('username').errors;

    return errors !== null;
  }

  municipalityHasErrors(): boolean {
    const errors: ValidationErrors = this.trainerForm.get('municipality').errors;

    return errors !== null;
  }

  cityHasErrors(): boolean {
    const errors: ValidationErrors = this.trainerForm.get('city').errors;

    return errors !== null;
  }

  emailHasErrors(): boolean {
    const errors: ValidationErrors = this.trainerForm.get('email').errors;

    return errors !== null;
  }

  dateOfBirthHasErrors(): boolean {
    const errors: ValidationErrors = this.trainerForm.get('dateOfBirth').errors;

    return errors !== null;
  }

  passwordHasErrors(): boolean {
    const errors: ValidationErrors = this.trainerForm.get('password').errors;

    return errors !== null;
  }

  genderHasErrors(): boolean {
    const errors: ValidationErrors = this.trainerForm.get('gender').errors;

    return errors !== null;
  }

  cnfPasswordHasErrors(): boolean {
    const errors: ValidationErrors = this.trainerForm.get('cnfPassword').errors;

    return errors !== null;
  }

  public priceHasErrors(): boolean {
    const errors: ValidationErrors = this.trainerForm.get('price').errors;
    console.log(errors)

    return errors !== null;
  }

  public surnameErrors(): string[] {
    const errors: ValidationErrors = this.trainerForm.get('surname').errors;
    if(errors === null) {
      return [];
    }

    const errorMessages: string[] = [];
    if (errors.required) {
      errorMessages.push('Trainer must have a last name.');
    }

    return errorMessages;
  }

  public usernameErrors(): string[] {
    const errors: ValidationErrors = this.trainerForm.get('username').errors;
    if(errors === null) {
      return [];
    }

    const errorMessages: string[] = [];
    if (errors.required) {
      errorMessages.push('Trainer must have a username.');
    }

    return errorMessages;
  }

  public emailErrors(): string[] {
    const errors: ValidationErrors = this.trainerForm.get('email').errors;
    if(errors === null) {
      return [];
    }

    const errorMessages: string[] = [];
    if (errors.required) {
      errorMessages.push('Trainer must have an email.');
    }
    if (errors.email) {
      errorMessages.push('Email must have a valid format.');
    }

    return errorMessages;
  }

  public municipalityErrors(): string[] {
    const errors: ValidationErrors = this.trainerForm.get('municipality').errors;
    if(errors === null) {
      return [];
    }

    const errorMessages: string[] = [];
    if (errors.required) {
      errorMessages.push('Nutritionist must have a municipality.');
    }

    return errorMessages;
  }

  public cityErrors(): string[] {
    const errors: ValidationErrors = this.trainerForm.get('city').errors;
    if(errors === null) {
      return [];
    }

    const errorMessages: string[] = [];
    if (errors.required) {
      errorMessages.push('Nutritionist must have a city.');
    }

    return errorMessages;
  }

  public dateOfBirthErrors(): string[] {
    const errors: ValidationErrors = this.trainerForm.get('dateOfBirth').errors;
    if(errors === null) {
      return [];
    }

    const errorMessages: string[] = [];
    if (errors.required) {
      errorMessages.push('Trainer must have a date of birth.');
    }

    return errorMessages;
  }

  public passwordErrors(): string[] {
    const errors: ValidationErrors = this.trainerForm.get('password').errors;
    if(errors === null) {
      return [];
    }

    const errorMessages: string[] = [];
    if (errors.required) {
      errorMessages.push('Trainer must have a first name.');
    }

    return errorMessages;
  }

  public cnfPasswordErrors(): string[] {
    const errors: ValidationErrors = this.trainerForm.get('cnfPassword').errors;
    if(errors === null) {
      return [];
    }

    const errorMessages: string[] = [];
    if (errors.required) {
      errorMessages.push('Password must be confirmed.');
    }

    return errorMessages;
  }

  public genderErrors(): string[] {
    const errors: ValidationErrors = this.trainerForm.get('gender').errors;
    if(errors === null) {
      return [];
    }

    const errorMessages: string[] = [];
    if (errors.required) {
      errorMessages.push('Gender must be chosen.');
    }

    return errorMessages;
  }
  public priceErrors(): string[] {
    const errors: ValidationErrors = this.trainerForm.get('price').errors;
    if(errors === null) {
      return [];
    }

    const errorMessages: string[] = [];
    if (errors.required) {
      errorMessages.push('Trainer must have a price');
    }
    if(errors.min){
      errorMessages.push('Price must be positive');
    }

    return errorMessages;
  }

}
